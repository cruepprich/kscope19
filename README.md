# KScope 19 Hands On Lab

These are the supporting files for the Hands On Lab at KScope 19.

## Useful Links

[Official Git Documentation](https://git-scm.com/doc)

[Katacoda Git Course](https://www.katacoda.com/courses/git)

[Make Your Git History Great Again](https://medium.com/@ahmedmahmoud.eltaher/make-your-git-history-great-again-da6a8ae799e0)

[A Successful Git Branching Model](https://nvie.com/posts/a-successful-git-branching-model/)

[Bump Version Script](https://gist.github.com/mareksuscak/1f206fbc3bb9d97dec9c)

[Cheat Sheet and Best Practices](https://www.git-tower.com/blog/git-cheat-sheet/)

[Atlassian Tutorials](https://www.atlassian.com/git/tutorials)


## Git Commands

* git add . - Add all modified files to staging area
* git add \<file\> - Add modified file to staging area
* git branch - Show branches
* git branch -d - Delete branch
* git branch -D - Delete branch with uncommited changes
* git checkout -- . - Revert all modified unstaged files to last commit.
* git checkout -- \<file\> - Revert unstaged file to last commit
* git checkout --ours - Accept local changes in merge conflict
* git checkout --thiers - Accept changes from branch in merge conflict
* git checkout -b \<branch\> - Create new branch and change to it
* git checkout \<branch\> - Change to branch
* git clone <URL> - Download an existing repository
* git config --global \<attribute\> \<value\> - Set global configuration variables
* git config --list --show-origin - Show configuration
* git diff - Compare unstaged changes with last commit
* git diff --cached - Compare stage area with last commit
* git init - Adds Git functionality to current directory
* git log - Shows commit history
* git merge --abort - Abort an ongoing merge
* git merge \<branch\> - Merge branch into current branch
* git pull - Pull latest changes from remote repository
* git push - Push local changes to remote repository
* git remote add origin \<URL\> - Connects local repository to remote
* git reset --hard \<commit hash\> - Go back to previous commit
* git reset \<file\> - Remove file from staging area.
* git status - Shows status of various files

